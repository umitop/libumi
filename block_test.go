// Copyright (c) 2020 UMI
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package libumi_test

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"testing"
	"time"

	"github.com/umitop/libumi"
)

func TestCalculateMerkleRoot(t *testing.T) {
	tests := []struct {
		count  int
		base64 string
	}{
		{1, "HYNRi4l7FOKUOZDv9lWDgkbMAgenyVpfPfzMLjlfi78="},
		{2, "5nxQuCEhLBP+XztSaepJ28qcgp/7ETPADXrqX8uZ38U="},
		{3, "304f5WJnRpBWJc8OM/GXElcq+9r4WzZB2GU3tJXrZZE="},
		{4, "0k7lgourOBJjkhrHeGVELXZbzsOaiMnnIApptve5oFc="},
		{5, "k24Xs6YvuR3cyoqKO+yBWeyaKbguywzkFTb7gG5mdGM="},
		{6, "JZRKgpSQd5p+LSJDiGzuMQ4mL9yYtBWkbpVqdUbAdk8="},
		{7, "gYekGUsQ3UdR171nY8OV8SLAf9dgNIe+yIBPErAwYnw="},
		{8, "Zn+VUCmI+ir8qmHlS+zaz9glnuJg2K3ZstWtNXzxxE0="},
	}

	for _, test := range tests {
		sec := make([]byte, 64)
		trx := libumi.NewTransactionBuilder()
		blk := libumi.NewBlockBuilder()

		for i := 0; i < test.count; i++ {
			for j := 0; j < 150; j++ {
				trx[j] = uint8(i)
			}

			blk.AppendTransaction(trx)
		}

		blk.Sign(sec)

		act := blk.Build().MerkleRootHash()
		exp, _ := base64.StdEncoding.DecodeString(test.base64)

		if !bytes.Equal(exp, act) {
			t.Fatalf("Expected: %x, got: %x", exp, act)
		}
	}
}

/*
func TestCalculateMerkleNonUniqError(t *testing.T) {
	trx := libumi.NewTransactionBuilder()
	blk := libumi.NewBlockBuilder()

	blk.AppendTransaction(trx)
	blk.AppendTransaction(trx)

	_, act := libumi.calculateMerkleRoot(blk)
	exp := libumi.ErrNonUniqueTx

	if !errors.Is(act, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}
*/

func TestBlockGenesis(t *testing.T) {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)

	tx := libumi.NewTransactionBuilder().
		SetVersion(libumi.Genesis).
		SetSender(libumi.NewAddressBuilder().SetPrefix("genesis").SetPublicKey(pub).Build()).
		SetRecipient(libumi.NewAddressBuilder().Build()).
		Sign(sec).
		Build()

	blk := libumi.NewBlockBuilder().SetVersion(libumi.Genesis)
	blk.AppendTransaction(tx)
	blk.Sign(sec)

	err := blk.Build().Verify()
	if err != nil {
		t.Fatalf("Expected: %v, got: %v", nil, err)
	}
}

func TestBlockBasic(t *testing.T) {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)

	tx := libumi.NewTransactionBuilder().
		SetVersion(libumi.Basic).
		SetSender(libumi.NewAddressBuilder().SetPrefix("umi").SetPublicKey(pub).Build()).
		SetRecipient(libumi.NewAddressBuilder().SetPrefix("aaa").Build()).
		Sign(sec).
		Build()

	blk := libumi.NewBlockBuilder()
	blk.SetVersion(libumi.Basic)
	blk.SetPreviousBlockHash(blk.Build().Hash())
	blk.AppendTransaction(tx)
	blk.Sign(sec)

	err := blk.Build().Verify()
	if err != nil {
		t.Fatalf("Expected: %v, got: %v", nil, err)
	}
}

func TestBlockLengthMustBeValid(t *testing.T) {
	cases := []struct {
		name string
		blk  libumi.Block
		exp  error
	}{
		{
			name: "blk must not be null",
			blk:  nil,
			exp:  libumi.ErrInvalidLength,
		},
		{
			name: "length must not be less then minimal",
			blk:  make([]byte, libumi.HeaderLength+libumi.TxLength-1),
			exp:  libumi.ErrInvalidLength,
		},
		{
			name: "length must match transaction count",
			blk: func() []byte {
				blk := make([]byte, libumi.HeaderLength+libumi.TxLength+1)
				binary.BigEndian.PutUint16(blk[69:71], 1)

				return blk
			}(),
			exp: libumi.ErrInvalidLength,
		},
	}

	for _, tc := range cases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.blk.Verify()

			if !errors.Is(err, tc.exp) {
				t.Fatalf("Expected: %v, got: %v", tc.exp, err)
			}
		})
	}
}

func TestBlockVersionMustBeValid(t *testing.T) {
	blk := libumi.NewBlockBuilder()
	blk.SetVersion(255)
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidVersion

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockSignatureMustBeValid(t *testing.T) {
	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidSignature

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockGenesisPrevBlockHashIsNull(t *testing.T) {
	_, sec, _ := ed25519.GenerateKey(rand.Reader)

	blk := libumi.NewBlockBuilder()
	blk.SetVersion(libumi.Genesis)
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.SetPreviousBlockHash(blk.Build().Hash())
	blk.Sign(sec)

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidPrevHash

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockGenesisCanContainOnlyGenesisTxs(t *testing.T) {
	_, sec, _ := ed25519.GenerateKey(rand.Reader)

	blk := libumi.NewBlockBuilder()
	blk.SetVersion(libumi.Genesis)
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.Sign(sec)

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidTx

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockBasicPrevBlockHashIsNotNull(t *testing.T) {
	_, sec, _ := ed25519.GenerateKey(rand.Reader)

	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.Sign(sec)

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidPrevHash

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockBasicCanNotContainGenesisTxs(t *testing.T) {
	_, sec, _ := ed25519.GenerateKey(rand.Reader)

	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(libumi.NewTransactionBuilder().SetVersion(libumi.Genesis).Build())
	blk.SetPreviousBlockHash(blk.Build().Hash())
	blk.Sign(sec)

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidTx

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockMustNotContainDuplicateTxs(t *testing.T) {
	_, sec, _ := ed25519.GenerateKey(rand.Reader)

	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.SetPreviousBlockHash(blk.Build().Hash())
	blk.Sign(sec)

	err := blk.Build().Verify()
	exp := libumi.ErrNonUniqueTx

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockMustHaveValidMerkleRoot(t *testing.T) {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)

	tx := libumi.NewTransactionBuilder().
		SetSender(libumi.NewAddressBuilder().SetPublicKey(pub).Build()).
		SetRecipient(libumi.NewAddressBuilder().Build()).
		Sign(sec).
		Build()

	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(tx)
	blk.SetPreviousBlockHash(blk.Build().Hash())

	blk.SetMerkleRootHash(blk.Build().Hash())
	blk.SetPublicKey(pub)
	blk.SetSignature(ed25519.Sign(sec, blk[0:103]))

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidMerkle

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestBlockMustContainValidTxs(t *testing.T) {
	_, sec, _ := ed25519.GenerateKey(rand.Reader)

	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.SetPreviousBlockHash(blk.Build().Hash())
	blk.Sign(sec)

	err := blk.Build().Verify()
	exp := libumi.ErrInvalidTx

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestSignBlock(t *testing.T) {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)
	blk := libumi.NewBlockBuilder()
	blk.AppendTransaction(libumi.NewTransactionBuilder().Build())
	blk.Sign(sec)

	act := blk.Build().PublicKey()

	if !bytes.Equal(act, pub) {
		t.Fatalf("Expected: %x, got: %x", pub, act)
	}
}

func TestBlock_Timestamp(t *testing.T) {
	exp := uint32(time.Now().Unix())
	blk := libumi.NewBlockBuilder().SetTimestamp(exp).Build()
	act := blk.Timestamp()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}

func TestBlock_Version(t *testing.T) {
	exp := libumi.Genesis
	blk := libumi.NewBlockBuilder().SetVersion(libumi.Genesis).Build()
	act := blk.Version()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}
