// Copyright (c) 2020 UMI
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package libumi_test

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"strings"
	"testing"

	"github.com/umitop/libumi"
)

type txCases struct {
	name string
	tx   libumi.Transaction
	exp  error
}

func newTx(ver uint8, sndPfx string, rcpPfx string) libumi.Transaction {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)

	return libumi.NewTransactionBuilder().
		SetVersion(ver).
		SetSender(libumi.NewAddressBuilder().SetPrefix(sndPfx).SetPublicKey(pub).Build()).
		SetRecipient(libumi.NewAddressBuilder().SetPrefix(rcpPfx).Build()).
		Sign(sec).
		Build()
}

func newTxStructBuilder() libumi.TransactionBuilder {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)

	return libumi.NewTransactionBuilder().
		SetVersion(libumi.CreateStructure).
		SetSender(libumi.NewAddressBuilder().SetPrefix("umi").SetPublicKey(pub).Build()).
		SetPrefix("abc").
		SetName("Hello World!").
		SetFeePercent(100).
		SetProfitPercent(500).
		Sign(sec)
}

func txTestCases(t *testing.T, cases []txCases) {
	for _, tc := range cases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			err := tc.tx.Verify()
			if !errors.Is(err, tc.exp) {
				t.Fatalf("Expected: %v, got: %v", tc.exp, err)
			}
		})
	}
}

func TestTransaction_ValidGenesis(t *testing.T) {
	tx := newTx(libumi.Genesis, "genesis", "umi")

	err := tx.Verify()
	if err != nil {
		t.Fatalf("Expected: %v, got: %v", nil, err)
	}
}

func TestTransaction_ValidBasic(t *testing.T) {
	tx := newTx(libumi.Basic, "umi", "aaa")

	err := tx.Verify()
	if err != nil {
		t.Fatalf("Expected: %v, got: %v", nil, err)
	}
}

func TestTransaction_ValidStruct(t *testing.T) {
	tx := newTxStructBuilder().Build()

	err := tx.Verify()
	if err != nil {
		t.Fatalf("Expected: %v, got: %v", nil, err)
	}
}

func TestTransaction_ValidAddress(t *testing.T) {
	pub, sec, _ := ed25519.GenerateKey(rand.Reader)

	tx := libumi.NewTransactionBuilder().
		SetVersion(libumi.CreateTransitAddress).
		SetSender(libumi.NewAddressBuilder().SetPrefix("umi").SetPublicKey(pub).Build()).
		SetRecipient(libumi.NewAddressBuilder().SetPrefix("abc").Build()).
		Sign(sec).
		Build()

	err := tx.Verify()
	if err != nil {
		t.Fatalf("Expected: %v, got: %v", nil, err)
	}
}

func TestTransaction_Length(t *testing.T) {
	cases := []txCases{
		{
			name: "null",
			tx:   nil,
			exp:  libumi.ErrInvalidLength,
		},
		{
			name: "too short",
			tx:   make([]byte, libumi.TxLength-1),
			exp:  libumi.ErrInvalidLength,
		},
		{
			name: "too long",
			tx:   make([]byte, libumi.TxLength+1),
			exp:  libumi.ErrInvalidLength,
		},
	}

	txTestCases(t, cases)
}

func TestTransaction_InvalidVersion(t *testing.T) {
	tx := libumi.NewTransactionBuilder().SetVersion(255).Build()

	err := tx.Verify()
	exp := libumi.ErrInvalidVersion

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestTransaction_InvalidGenesis(t *testing.T) {
	cases := []txCases{
		{
			name: "sender must be genesis",
			tx:   newTx(libumi.Genesis, "bbb", "umi"),
			exp:  libumi.ErrInvalidSender,
		},
		{
			name: "recipient must be umi",
			tx:   newTx(libumi.Genesis, "genesis", "cde"),
			exp:  libumi.ErrInvalidRecipient,
		},
	}

	txTestCases(t, cases)
}

func TestTransaction_InvalidBasic(t *testing.T) {
	cases := []txCases{
		{
			name: "sender prefix must be valid",
			tx:   newTx(libumi.Basic, "}}}", "umi"),
			exp:  libumi.ErrInvalidSender,
		},
		{
			name: "sender and recipient must be not equal",
			tx: libumi.NewTransactionBuilder().
				SetSender(libumi.NewAddressBuilder().Build()).
				SetRecipient(libumi.NewAddressBuilder().Build()).
				Build(),
			exp: libumi.ErrInvalidRecipient,
		},
		{
			name: "sender must be not genesis",
			tx:   newTx(libumi.Basic, "genesis", "qwe"),
			exp:  libumi.ErrInvalidSender,
		},
		{
			name: "recipient must be not genesis",
			tx:   newTx(libumi.Basic, "ghj", "genesis"),
			exp:  libumi.ErrInvalidRecipient,
		},
		{
			name: "recipient prefix must be valid",
			tx:   newTx(libumi.Basic, "umi", "{{{"),
			exp:  libumi.ErrInvalidRecipient,
		},
	}

	txTestCases(t, cases)
}

func TestTransaction_InvalidAddress(t *testing.T) {
	cases := []txCases{
		{
			name: "sender must be umi",
			tx:   newTx(libumi.CreateTransitAddress, "aaa", "asd"),
			exp:  libumi.ErrInvalidSender,
		},
		{
			name: "recipient must not be genesis",
			tx:   newTx(libumi.CreateTransitAddress, "umi", "genesis"),
			exp:  libumi.ErrInvalidRecipient,
		},
		{
			name: "recipient must not be umi",
			tx:   newTx(libumi.CreateTransitAddress, "umi", "umi"),
			exp:  libumi.ErrInvalidRecipient,
		},
		{
			name: "recipient prefix must be valid",
			tx:   newTx(libumi.CreateTransitAddress, "umi", "|||"),
			exp:  libumi.ErrInvalidRecipient,
		},
	}

	txTestCases(t, cases)
}

func TestTransaction_InvalidStruct(t *testing.T) {
	cases := []txCases{
		{
			name: "sender must be umi",
			tx:   newTxStructBuilder().SetSender(libumi.NewAddressBuilder().SetPrefix("aaa").Build()).Build(),
			exp:  libumi.ErrInvalidSender,
		},
		{
			name: "prefix can not be umi",
			tx:   newTxStructBuilder().SetPrefix("umi").Build(),
			exp:  libumi.ErrInvalidPrefix,
		},
		{
			name: "prefix can not be genesis",
			tx:   newTxStructBuilder().SetPrefix("genesis").Build(),
			exp:  libumi.ErrInvalidPrefix,
		},
		{
			name: "prefix must be valid",
			tx:   newTxStructBuilder().SetPrefix("}}}").Build(),
			exp:  libumi.ErrInvalidPrefix,
		},
		{
			name: "profit percent must be between 1_00 and 5_00",
			tx:   newTxStructBuilder().SetProfitPercent(10_00).Build(),
			exp:  libumi.ErrInvalidProfitPercent,
		},
		{
			name: "fee percent must be between 0 and 20_00",
			tx:   newTxStructBuilder().SetFeePercent(23_45).Build(),
			exp:  libumi.ErrInvalidFeePercent,
		},
		{
			name: "name length must be 35 bytes or less",
			tx:   newTxStructBuilder().SetName(strings.Repeat("a", 36)).Build(),
			exp:  libumi.ErrInvalidName,
		},
		{
			name: "name must be valid UTF-8 string",
			tx:   newTxStructBuilder().SetName("\xff\x01\x01\x01").Build(),
			exp:  libumi.ErrInvalidName,
		},
	}

	txTestCases(t, cases)
}

func TestTransaction_InvalidSignature(t *testing.T) {
	tx := newTxStructBuilder().SetPrefix("cba").Build()

	err := tx.Verify()
	exp := libumi.ErrInvalidSignature

	if !errors.Is(err, exp) {
		t.Fatalf("Expected: %v, got: %v", exp, err)
	}
}

func TestTransaction_Value(t *testing.T) {
	exp := uint64(42)
	adr := libumi.NewTransactionBuilder().SetValue(exp).Build()
	act := adr.Value()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}

func TestTransaction_Name(t *testing.T) {
	exp := "Hello World 😊"
	adr := libumi.NewTransactionBuilder().SetName(exp).Build()
	act := adr.Name()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}

func TestTransaction_Prefix(t *testing.T) {
	exp := "zzz"
	adr := libumi.NewTransactionBuilder().SetPrefix(exp).Build()
	act := adr.Prefix()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}

func TestTransaction_Version(t *testing.T) {
	exp := libumi.DeleteTransitAddress
	adr := libumi.NewTransactionBuilder().SetVersion(exp).Build()
	act := adr.Version()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}

func TestTransaction_Hash(t *testing.T) {
	adr := libumi.NewTransactionBuilder().Build()
	exp := sha256.Sum256(adr)
	act := adr.Hash()

	if !bytes.Equal(act, exp[:]) {
		t.Fatalf("Expected: %x, got: %x", exp, act)
	}
}

func TestTransaction_Nonce(t *testing.T) {
	exp := uint64(42)
	adr := libumi.NewTransactionBuilder().SetNonce(exp).Build()
	act := adr.Nonce()

	if act != exp {
		t.Fatalf("Expected: %v, got: %v", exp, act)
	}
}

func TestTransaction_Signature(t *testing.T) {
	adr := libumi.NewTransactionBuilder().Sign(make([]byte, 64)).Build()
	act := adr.Signature()

	if bytes.Equal(act, make([]byte, 64)) {
		t.Fatalf("Expected non empty signature")
	}
}
